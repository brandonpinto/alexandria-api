import { PostReview } from '@/domain/usecases'
import { HttpResponse, Controller } from '@/presentation/contracts'
import { MissingParamError } from '@/presentation/errors'
import { badRequest, noContent, serverError } from '@/presentation/helpers'

export class PostReviewController implements Controller {
  constructor(private readonly postReview: PostReview) {}

  async perform(request: PostReviewController.Request): Promise<HttpResponse> {
    try {
      const params = ['book', 'rating', 'review']
      for (const field of params) {
        if (!request[field]) {
          return badRequest(new MissingParamError(field))
        }
      }
      await this.postReview.post(request)
      return noContent()
    } catch (err) {
      return serverError(err)
    }
  }
}

export namespace PostReviewController {
  export type Request = {
    book: string
    rating: number
    review: string
  }
}
