import { PostReview } from '@/domain/usecases'
import { PostReviewRepository } from '@/data/contracts'

export class DbPostReview implements PostReview {
  constructor(private readonly postReviewRepository: PostReviewRepository) {}

  async post(data: PostReview.Params): Promise<void> {
    await this.postReviewRepository.post(data)
  }
}
