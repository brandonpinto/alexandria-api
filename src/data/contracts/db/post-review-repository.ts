import { PostReview } from '@/domain/usecases'

export interface PostReviewRepository {
  post: (params: PostReviewRepository.Params) => Promise<void>
}

export namespace PostReviewRepository {
  export type Params = PostReview.Params
}
