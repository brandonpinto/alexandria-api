import { PostReviewRepository } from '@/data/contracts'
import { MongoHelper } from '@/infra/db'

export class ReviewMongoRepository implements PostReviewRepository {
  async post(data: PostReviewRepository.Params): Promise<void> {
    const reviewCollection = MongoHelper.getCollection('reviews')
    await reviewCollection.insertOne(data)
  }
}
