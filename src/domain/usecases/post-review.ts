export interface PostReview {
  post: (params: PostReview.Params) => Promise<void>
}

export namespace PostReview {
  export type Params = {
    book: string
    rating: number
    review: string
  }
}
