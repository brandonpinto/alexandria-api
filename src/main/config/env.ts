export default {
  mongo_url: process.env.MONGO_URL ?? 'mongodb://127.0.0.1:27017/api',
  port: process.env.PORT ?? 8000
}
