export const postReviewParamsSchema = {
  type: 'object',
  properties: {
    book: {
      type: 'string'
    },
    rating: {
      type: 'number'
    },
    review: {
      type: 'string'
    }
  },
  required: ['book', 'rating', 'review']
}
