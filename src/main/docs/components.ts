import { serverError, notFound } from './components/'

export default {
  serverError,
  notFound
}
