import paths from './paths'
import schemas from './schemas'
import components from './components'

export default {
  openapi: '3.0.0',
  info: {
    title: 'Alexandria API',
    version: '0.0.1'
  },
  servers: [
    {
      url: '/api',
      description: 'Main Server'
    }
  ],
  tags: [
    {
      name: 'Reviews',
      description: 'Review related APIs'
    }
  ],
  paths,
  schemas,
  components
}
