import { reviewPath } from './paths/'

export default {
  '/reviews': reviewPath
}
