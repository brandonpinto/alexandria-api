import { postReviewParamsSchema, errorSchema } from './schemas/'

export default {
  error: errorSchema,
  postReviewParams: postReviewParamsSchema
}
