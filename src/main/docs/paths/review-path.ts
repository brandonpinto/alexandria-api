export const reviewPath = {
  post: {
    tags: ['Reviews'],
    summary: 'Post Review API',
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/schemas/postReviewParams'
          }
        }
      }
    },
    responses: {
      204: {
        description: 'Success, but no content'
      },
      404: {
        $ref: '#/components/notFound'
      },
      500: {
        $ref: '#/components/serverError'
      }
    }
  }
}
