import { Controller } from '@/presentation/contracts'
import { PostReviewController } from '@/presentation/controllers'
import { makeDbPostReview } from '@/main/factories'

export const makePostReviewController = (): Controller => {
  return new PostReviewController(makeDbPostReview())
}
