import { DbPostReview } from '@/data/usecases'
import { ReviewMongoRepository } from '@/infra/db'

export const makeDbPostReview = (): DbPostReview => {
  const reviewMongoRepository = new ReviewMongoRepository()
  return new DbPostReview(reviewMongoRepository)
}
