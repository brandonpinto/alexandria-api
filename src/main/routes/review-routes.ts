import { Router } from 'express'

import { adaptRoute } from '@/main/adapters'
import { makePostReviewController } from '@/main/factories'

export default (router: Router): void => {
  router.post('/reviews', adaptRoute(makePostReviewController()))
}
