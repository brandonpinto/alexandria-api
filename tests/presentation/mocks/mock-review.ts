import { PostReview } from '@/domain/usecases'

export class PostReviewMock implements PostReview {
  params: PostReview.Params

  async post(params: PostReview.Params): Promise<void> {
    this.params = params
  }
}
