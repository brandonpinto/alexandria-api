import { faker } from '@faker-js/faker'

import { PostReviewController } from '@/presentation/controllers'
import { PostReviewMock } from '@/tests/presentation/mocks'
import { MissingParamError, ServerError } from '@/presentation/errors'
import { badRequest, noContent, serverError } from '@/presentation/helpers'
import { throwError } from '@/tests/domain/mocks'

const mockRequest = (): PostReviewController.Request => ({
  book: faker.random.words(),
  rating: faker.datatype.number(),
  review: faker.random.words()
})

type SutTypes = {
  sut: PostReviewController
  postReviewMock: PostReviewMock
}

const makeSut = (): SutTypes => {
  const postReviewMock = new PostReviewMock()
  const sut = new PostReviewController(postReviewMock)
  return { sut, postReviewMock }
}

describe('PostReview Controller', () => {
  it('should return 204 on success', async () => {
    const { sut } = makeSut()
    const res = await sut.perform(mockRequest())
    expect(res).toEqual(noContent())
  })

  it('should return 400 if no book is provided', async () => {
    const { sut } = makeSut()
    const req = mockRequest()
    const res = await sut.perform({
      rating: req.rating,
      review: req.review
    } as any)
    expect(res).toEqual(badRequest(new MissingParamError('book')))
  })

  it('should return 400 if no rating is provided', async () => {
    const { sut } = makeSut()
    const req = mockRequest()
    const res = await sut.perform({
      book: req.book,
      review: req.review
    } as any)
    expect(res).toEqual(badRequest(new MissingParamError('rating')))
  })

  it('should return 400 if no review is provided', async () => {
    const { sut } = makeSut()
    const req = mockRequest()
    const res = await sut.perform({
      book: req.book,
      rating: req.rating
    } as any)
    expect(res).toEqual(badRequest(new MissingParamError('review')))
  })

  it('should call PostReview with correct values', async () => {
    const { sut, postReviewMock } = makeSut()
    const req = mockRequest()
    await sut.perform(req)
    expect(postReviewMock.params).toEqual(req)
  })

  it('should return 500 if PostReview throws', async () => {
    const { sut, postReviewMock } = makeSut()
    jest.spyOn(postReviewMock, 'post').mockImplementationOnce(throwError)
    const res = await sut.perform(mockRequest())
    expect(res).toEqual(serverError(new ServerError(null)))
  })
})
