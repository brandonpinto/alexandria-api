import { PostReviewRepository } from '@/data/contracts'

export class PostReviewRepositoryMock implements PostReviewRepository {
  params: PostReviewRepository.Params

  async post(params: PostReviewRepository.Params): Promise<void> {
    this.params = params
  }
}
