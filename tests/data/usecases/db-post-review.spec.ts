import { DbPostReview } from '@/data/usecases'
import { PostReviewRepositoryMock } from '@/tests/data/mocks'
import { mockPostReviewParams, throwError } from '@/tests/domain/mocks'

type SutTypes = {
  sut: DbPostReview
  postReviewRepositoryMock: PostReviewRepositoryMock
}

const makeSut = (): SutTypes => {
  const postReviewRepositoryMock = new PostReviewRepositoryMock()
  const sut = new DbPostReview(postReviewRepositoryMock)
  return { sut, postReviewRepositoryMock }
}

describe('DbPostReview Usecase', () => {
  it('should call PostReviewRepository with correct values', async () => {
    const { sut, postReviewRepositoryMock } = makeSut()
    const data = mockPostReviewParams()
    await sut.post(data)
    expect(postReviewRepositoryMock.params).toEqual(data)
  })

  it('should throw if PostReviewRepository throws', async () => {
    const { sut, postReviewRepositoryMock } = makeSut()
    jest
      .spyOn(postReviewRepositoryMock, 'post')
      .mockImplementationOnce(throwError)
    const promise = sut.post(mockPostReviewParams())
    await expect(promise).rejects.toThrow()
  })
})
