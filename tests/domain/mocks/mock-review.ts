import { faker } from '@faker-js/faker'

import { PostReview } from '@/domain/usecases'

export const mockPostReviewParams = (): PostReview.Params => ({
  book: faker.random.words(),
  rating: faker.datatype.number(),
  review: faker.random.words()
})
