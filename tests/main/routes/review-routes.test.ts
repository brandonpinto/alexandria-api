import { Express } from 'express'
import { Collection } from 'mongodb'
import request from 'supertest'

import { setupApp } from '@/main/config/app'
import { MongoHelper } from '@/infra/db'

let reviewCollection: Collection
let app: Express

describe('Review Routes', () => {
  beforeAll(async () => {
    app = await setupApp()
    await MongoHelper.connect(process.env.MONGO_URL)
  })

  beforeEach(async () => {
    reviewCollection = MongoHelper.getCollection('reviews')
    await reviewCollection.deleteMany({})
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  describe('POST /reviews', () => {
    it('should return 204 on success', async () => {
      await request(app)
        .post('/api/reviews')
        .send({
          book: 'any_book',
          rating: 10,
          review: 'any_review'
        })
        .expect(204)
    })
  })
})
