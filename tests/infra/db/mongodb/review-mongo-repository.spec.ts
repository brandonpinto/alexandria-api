import { Collection } from 'mongodb'

import { MongoHelper, ReviewMongoRepository } from '@/infra/db'
import { mockPostReviewParams } from '@/tests/domain/mocks'

const makeSut = (): ReviewMongoRepository => new ReviewMongoRepository()

let reviewCollection: Collection

describe('ReviewMongoRepository', () => {
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL)
  })

  beforeEach(async () => {
    reviewCollection = MongoHelper.getCollection('reviews')
    await reviewCollection.deleteMany({})
  })

  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  describe('post()', () => {
    it('should post a review on success', async () => {
      const sut = makeSut()
      await sut.post(mockPostReviewParams())
      const count = await reviewCollection.countDocuments()
      expect(count).toBe(1)
    })
  })
})
